﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex23
{
    class Program
    {
        static void Main(string[] args)

        /// Task A ///
        {
           Console.WriteLine("");
           var newRelease = new Dictionary<string, string>();

           newRelease.Add("Fast & Furious 8", "Action, Crime, Thriller");
           newRelease.Add("Beauty and the Beast", "Family, Fantasy, Musical");
           newRelease.Add("Kong: Skull Island", "Action, Adventure, Fantasy");
           newRelease.Add("Boss Baby", "Animation, Comedy, Family");
           newRelease.Add("Ghost in the Shell", "Action, Drama, Sci-Fi ");

           //var output = "";
           //bool value = newRelease.TryGetValue("Fast & Furious 8", out output);

           foreach(var movies in newRelease)
           {
               Console.WriteLine($"The movie title '{movies.Key}', and the genre '{movies.Value}'");   //(key = title, value = genre)
               Console.WriteLine("");
           }
           /// Task B ///

           //Console.WriteLine("");
           
            //foreach(var movies in newRelease)
            //{
                //newRelease.Sort();
            //}
            
           /// Task C ///

           Console.WriteLine("");
           int genreComedies = 0;
           int genreAction = 0;
           int genreDrama = 0;

           foreach(var movies in newRelease)
           {
               Console.WriteLine(movies.Value);
               switch(movies.Value.ToString().ToLower())
                {
                    case "Comedy"   : ++genreComedies; break;
                    case "Action"   : ++genreAction; break;
                    case "Drama"    : ++genreDrama; break;
                    default:break;
               }
           }
        
           Console.WriteLine($"Comdey Movies : {genreComedies}");
           Console.WriteLine($"Action Movies : {genreAction}");
           Console.WriteLine($"Drama Movies : {genreDrama}");

           /// Task D ///
           var addMovie = true;

           do
           {
               Console.WriteLine($"Type in the title of the movie to add : ");
               var movieTitle = Console.ReadLine();

               Console.WriteLine($"Type in the genre of the movie to add : ");
               var movieGenre = Console.ReadLine();

               Console.WriteLine($"Do you want to add another movie ? (yes or no) : ");
               addMovie = (Console.ReadLine() == "yes") ? false : true;

           } while(!addMovie);

           //print
           foreach(var movies in newRelease)
           {
                Console.WriteLine($"The movie is {movies.Key}, genre is {movies.Value}");
           }
        }
    }
}
    
   